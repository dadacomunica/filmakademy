<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'filmakad_LsxPwS');

/** Tu nombre de usuario de MySQL */
//define('DB_USER', 'filmakad_LsxPwS');
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
//define('DB_PASSWORD', '5eFRWD9dCIqU');
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$A:Mb}*M/}gFD~0yP. B)G|L&>T4+K$*;C`fbi/~`?I+htM8Pc+.pG][:c9W.XGF');
define('SECURE_AUTH_KEY',  'B/fI=cman9Wo+eoBj@Hi{-Zx}KV yqs_J9u0(z<sJWw]0%40!s(cL|U5K-mEW7|)');
define('LOGGED_IN_KEY',    '.(m9lp4YQwa|b@1OSH8}^){hpDpO,^=n~!{rv^=CTMf/-RJX&tzGw+#r&l!`t2?Q');
define('NONCE_KEY',        '&A(<ps=eXG3I$DHIF+{0<4$tAZB-w]:f/Rs#gSpX4t/^Ua-u&8(*07C&cNk;-0a:');
define('AUTH_SALT',        '_fnWeF3d1u4%#}csMYb0]w<-qF+m}L=0e?PdgXgCh R}m_nG+>:i=b,_yZ/)^HmE');
define('SECURE_AUTH_SALT', '>++hi6fiA30jsd$wq%).SIAUqKoG(|fxclS4)+PlN|^qE|)b%+eP$+3)9->82;y1');
define('LOGGED_IN_SALT',   'Tk|4n}4OOrgc|E;3S5p1!sxjA~1qZHdk@2G5)m8|j]}rxuMCo+YAZ7h83N@mFN|D');
define('NONCE_SALT',       'RO|]gXB!/ kh5G|rXv47J_r<UXbmrGC=Gckv00r@+t6)q;Ymxe~~@J)^fw(_DC]<');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'fakprvp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);
//Todas las actualizaciones del núcleo desactivadas
define( 'WP_AUTO_UPDATE_CORE', false );
/*disallow file edit*/
define(‘DISALLOW_FILE_EDIT’, true);
/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

