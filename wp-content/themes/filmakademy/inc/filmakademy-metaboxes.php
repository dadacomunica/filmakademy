<?php
function filmakademy_meta_box_film( $meta_boxes ) {
    $prefix = 'film-';

    $meta_boxes[] = array(
        'id' => 'film-parent',
        'title' => esc_html__( 'Parent page', 'film-metabox' ),
        'post_types' => array( 'film' ),
        'context' => 'side',
        'priority' => 'high',
        'autosave' => true,
        'fields' => array(
            array(
                'id' => $prefix . 'parent',
                'type' => 'post',
                'post_type' => 'page',
            ),
        ),
    );
    /*$meta_boxes[] = array(
        'id' => 'film-secondtext',
        'title' => esc_html__( 'Second text', 'film-metabox' ),
        'post_types' => array( 'film' ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'fields' => array(
            array(
                'id' => $prefix . 'seconddetails',
                'type' => 'wysiwyg',
            ),
        ),
    );*/
    $meta_boxes[] = array(
        'id' => 'film-movies',
        'title' => esc_html__( 'Movies', 'film-metabox' ),
        'post_types' => array( 'film' ),
        'context' => 'normal',
        'priority' => 'high',
        'autosave' => true,
        'fields' => array(
            array(
                'name'    => 'Featured',
                'id' => $prefix . 'featured-movies',
                'type' => 'post',
                'post_type' => 'movie',
                'field_type' => 'select_advanced',
                'multiple' => true,
            ),
            array(
                'name'    => 'Others',
                'id' => $prefix . 'movies',
                'type' => 'post',
                'post_type' => 'movie',
                'field_type' => 'select_advanced',
                'multiple' => true,
            ),
        ),
    );

    return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'filmakademy_meta_box_film' );
