<?php
$enable_breadcrumbs = 'no';
if(get_post_meta($id, "qode_enable_breadcrumbs", true) != ""){
    $enable_breadcrumbs = get_post_meta($id, "qode_enable_breadcrumbs", true);
}elseif(isset($qode_options['enable_breadcrumbs'])){
    $enable_breadcrumbs = $qode_options['enable_breadcrumbs'];
} ?>
<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

        <div class="title_outer title_without_animation">
            <div class="title position_left has_fixed_background standard_title">
                <div class="title_holder">
                    <div class="container">
                        <div class="container_inner clearfix">
                            <div class="title_subtitle_holder">
                                <div class="title_subtitle_holder_inner two_columns_50_50">
                                    <h1 class="column1"><span><?php echo get_the_title(); ?></span></h1>
                                    <?php if (function_exists('filmakademy_custom_breadcrumbs') && $enable_breadcrumbs == "yes") { ?>
                                        <div class="breadcrumb column2"> <?php filmakademy_custom_breadcrumbs(); ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="full_width section">
            <div class="full_width_inner grid_section">
                <div class="section_inner clearfix">
                    <p><?php echo do_shortcode(get_the_content()); ?></p>
                </div>
                <div class="section_inner full_page_container_inner">
                    <?php $post_ids = rwmb_meta( 'film-featured-movies' ); ?>
                    <?php $i = 1; ?>
                    <?php foreach ( $post_ids as $post_id ): ?>
                        <?php $content = strip_tags(get_post_field('post_content', $post_id)); ?>
                        <div class="featured clearfix two_columns_33_66">
                            <div class="featured_image column1">
                                <div class="column_inner">
                                    <a href="<?php echo get_post_permalink( $post_id ); ?>" title="Enlace a <?php echo get_the_title( $post_id ); ?>">
                                        <?php echo get_the_post_thumbnail( $post_id ); ?>
                                    </a>
                                </div>
                            </div>
                            <div class="featured_content column2">
                                <div class="column_inner">
                                    <a href="<?php echo get_post_permalink( $post_id ); ?>" title="Enlace a <?php echo get_the_title( $post_id ); ?>">
                                        <h2><?php echo get_the_title( $post_id ); ?></h2>
                                        <h3><?php echo WPMOLY_Movies::get_movie_meta( $post_id, 'director' ); ?></h3>
                                        <span><?php echo substr(WPMOLY_Movies::get_movie_meta( $post_id, 'release_date' ), 0, 4); ?></span>
                                        <?php if($content!=''): ?>
                                            <p><?php echo wp_trim_words($content, 90); ?></p>
                                        <?php else: ?>
                                            <p><?php echo wp_trim_words(WPMOLY_Movies::get_movie_meta( $post_id, 'overview' ), 90); ?></p>
                                        <?php endif; ?>
                                        <div class="button"><span>Leer más</span></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <main class="section_inner clearfix">
                    <p><?php echo do_shortcode(rwmb_meta( 'film-seconddetails' )); ?></p>
                </main>
                <div class="section_inner full_page_container_inner clearfix four_columns">
                    <?php $post_ids = rwmb_meta( 'film-movies' ); ?>
                    <?php $i = 1; ?>
                    <?php foreach ( $post_ids as $post_id ): ?>
                        <div class="movie column<?php echo $i; ?>">
                            <div class="column_inner">
                                <div class="movie_image">
                                    <a href="<?php echo get_post_permalink( $post_id ); ?>" title="Enlace a <?php echo get_the_title( $post_id ); ?>">
                                        <?php echo get_the_post_thumbnail( $post_id ); ?>
                                    </a>
                                </div>
                                <a href="<?php echo get_post_permalink( $post_id ); ?>" title="Enlace a <?php echo get_the_title( $post_id ); ?>">
                                    <div class="movie_content">
                                        <h2><?php echo get_the_title( $post_id ); ?></h2>
                                        <h3><?php echo WPMOLY_Movies::get_movie_meta( $post_id, 'director' ); ?></h3>
                                        <span><?php echo substr(WPMOLY_Movies::get_movie_meta( $post_id, 'release_date' ), 0, 4); ?></span>
                                        <p><?php echo wp_trim_words(WPMOLY_Movies::get_movie_meta( $post_id, 'overview' ), 25); ?></p>
                                        <div class="button"><span>Leer más</span></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php $i++; ?>
                        <?php if($i==5) $i = 1; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php else : ?>
    <?php _e('No posts were found.', 'us'); ?>
<?php endif; ?>
<?php get_footer(); ?>