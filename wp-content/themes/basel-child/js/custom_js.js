var $j = jQuery.noConflict();

$j(document).ready(function() {
    $j("#theyear").text( (new Date).getFullYear() );
    $j(".foldedText").readmore({
        speed: 500,
        moreLink: '<a href="#">Leer más</a>',
        lessLink: '<a href="#">Leer menos</a>'
	});
});