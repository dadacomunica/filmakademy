<?php
$enable_breadcrumbs = 'no';
if(get_post_meta($id, "qode_enable_breadcrumbs", true) != ""){
    $enable_breadcrumbs = get_post_meta($id, "qode_enable_breadcrumbs", true);
}elseif(isset($qode_options['enable_breadcrumbs'])){
    $enable_breadcrumbs = $qode_options['enable_breadcrumbs'];
} ?>
<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

        <h1 class="column1 title-large"><span><?php echo get_the_title(); ?></span></h1>
        <?php if (function_exists('filmakademy_custom_breadcrumbs') && $enable_breadcrumbs == "yes") { ?>
        <div class="breadcrumb column2"> <?php filmakademy_custom_breadcrumbs(); ?></div>
        <?php } ?>
                                
        <section class="full_width section list-margin-top">
            <div class="full_width_inner grid_section">
                <div class="section_inner full_page_container_inner clearfix two_columns_50_50">
                    <div class="row fitxa">
                        <div class="poster column1 col-md-4">
                            <?php the_post_thumbnail() ?>
                        </div>
                        <div class="data column2 col-md-8">
                            <h2><?php echo __('Título original'); ?>: <?php echo rwmb_meta('_wpmoly_movie_original_title' ); ?></h2>
                            <div class="details">
                                <?php echo rwmb_meta('_wpmoly_movie_director' ); ?>.
                                <?php echo substr(rwmb_meta('_wpmoly_movie_release_date' ), 0, 4); ?>.
                                <?php echo rwmb_meta('_wpmoly_movie_production_countries' ); ?>.
                            </div>
                            <div class="tagcloud">
                                <?php $genres = get_the_terms( get_the_ID(),'genre'); ?>
                                <?php foreach($genres as $genre): ?>
                                    <span><?php echo $genre->name; ?></span>
                                <?php endforeach; ?>
                            </div>
                            <?php $the_content = get_the_content(); ?>
                            <?php if($the_content != ''): ?>
                                <div class="overview"><?php echo wpautop($the_content); ?></div>
                            <?php else: ?>
                                <div class="overview"><?php echo wpautop(rwmb_meta('_wpmoly_movie_overview' )); ?></div>
                            <?php endif; ?>
                        </div>
                        <div class="sharer">
                            <a href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank" class="q_social_icon_holder with_link">
                                <span class="fa-stack tiny circle_social facebook">
                                    <span class="social_icon social_facebook"></span>
                                </span>
                            </a>
                            <a href="https://twitter.com/share?url=<?php echo get_the_permalink(); ?>&amp;text=<?php echo get_the_title(); ?>%20-%20FilmAkademy&amp;hashtags=filmakademy" target="_blank" class="q_social_icon_holder with_link">
                                <span class="fa-stack tiny circle_social twitter">
                                    <span class="social_icon social_twitter"></span>
                                </span>
                            </a>
                            <a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" target="_blank" class="q_social_icon_holder with_link">
                                <span class="fa-stack tiny circle_social googleplus">
                                    <span class="social_icon social_googleplus"></span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="full_width section">
            <div class="full_width_inner grid_section">
                <div class="section_inner full_page_container_inner clearfix">
                    <?php comments_template(); ?>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php else : ?>
    <?php _e('No posts were found.', 'us'); ?>
<?php endif; ?>
<?php get_footer(); ?>