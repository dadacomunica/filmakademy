<?php 
if ( ! defined( 'FILMAKADEMY_BASE_FILE' ) )
    define( 'FILMAKADEMY_BASE_FILE', __FILE__ );
if ( ! defined( 'FILMAKADEMY_BASE_DIR' ) )
    define( 'FILMAKADEMY_BASE_DIR', dirname( FILMAKADEMY_BASE_FILE ) );
if ( ! defined( 'FILMAKADEMY_THEME_URL' ) )
    define( 'FILMAKADEMY_THEME_URL', get_template_directory_uri() );

require_once(FILMAKADEMY_BASE_DIR . '/inc/filmakademy-types-taxonomies.php');
require_once(FILMAKADEMY_BASE_DIR . '/inc/filmakademy-metaboxes.php');
require_once(FILMAKADEMY_BASE_DIR . '/inc/filmakademy-styles.php');