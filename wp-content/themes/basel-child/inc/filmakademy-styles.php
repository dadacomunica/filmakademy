<?php
function filmakademy_styles()
{
//    if( is_front_page()):
//    endif;
    wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
    wp_enqueue_style( 'header-style', get_stylesheet_directory_uri() . '/css/header.css' );
    wp_enqueue_style( 'home-style', get_stylesheet_directory_uri() . '/css/home.css' );
    wp_enqueue_style( 'min-style', get_stylesheet_directory_uri() . '/css/stylesheet.min.css' );
    wp_enqueue_style( 'childstyle' );
    wp_enqueue_script('readmore_jquery', get_stylesheet_directory_uri().'/js/readmore.min.js');
    wp_enqueue_script('child_theme_script_handle', get_stylesheet_directory_uri().'/js/custom_js.js');
//    if(get_post_type() == 'filmakademy'):
//
//    endif;
}
add_action('wp_enqueue_scripts', 'filmakademy_styles');

function filmakademy_dequeue()
{
    wp_deregister_style('qode_stylesheet');
    wp_dequeue_style('qode_stylesheet');
}
add_action('wp_print_styles', 'filmakademy_dequeue');

/*
function filmakademy_theme_setup() {
    load_child_theme_textdomain( 'stockholm', get_stylesheet_directory_uri() . '/languages/' );
}
*/

add_action( 'after_setup_theme', 'filmakademy_theme_setup', 22);

/*deny iframes*/
add_action( 'send_headers', 'add_header_xframeoptions' );

function add_header_xframeoptions() {
    header( 'X-Frame-Options: SAMEORIGIN' );
}