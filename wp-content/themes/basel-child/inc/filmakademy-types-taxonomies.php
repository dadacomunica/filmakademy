<?php
/**
 * Custom post type for Film lists
 */
function filmakademy_register() {
    register_post_type('film',
        [
            'labels' => [
                'name' => __('Subcategories'),
                'singular_name' => __('Subcategory'),
            ],
            'public' => true,
            'menu_icon' => 'dashicons-video-alt',
            'has_archive' => false,
            'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
            'hierarchical' => true,
            'capability_type' => 'post',
            'publicly_queryable' => true,
            'rewrite' => [
                'slug' => '%film_parent%'
            ],
            'menu_position' => 8,
            'show_in_nav_menus' => true,
        ]
    );

//    $film_category_labels = [
//        'name'              => _x('Film Categories', 'film-categories'),
//        'singular_name'     => _x('Film Category', 'film-category'),
//        'edit_item'         => __('Edit Film Category'),
//        'update_item'       => __('Update Film Category'),
//        'add_new_item'      => __('Add New Film Category'),
//        'new_item_name'     => __('New Film Category Name'),
//        'menu_name'         => __('Film Categories'),
//    ];
//    $film_category = [
//        'hierarchical'      => false,
//        'labels'            => $film_category_labels,
//        'show_ui'           => true,
//        'show_admin_column' => true,
//        'query_var'         => true,
//    ];
//    register_taxonomy('film-categories', 'film', $film_category);

    flush_rewrite_rules();
}
add_action('init', 'filmakademy_register');

/**
 * @param $rules
 * @return array
 * Custom rewrite rule for URL of Film lists
 */
function filmakademy_add_rewrite_rules( $rules ) {
    $new = array();
    $query = new WP_Query(array(
        'post_type' => 'film',
        'post_status' => 'publish'
    ));
    while ($query->have_posts()):
        $query->the_post();
        $post_parent_id = rwmb_meta( 'film-parent' );
        if($post_parent_id != ''):
            $post_parent = get_post($post_parent_id);
            $new[$post_parent->post_name.'/(.+)/?$'] = 'index.php?film=$matches[1]';
        endif;
    endwhile;

    return array_merge( $new, $rules ); // Ensure our rules come first
}
add_filter( 'rewrite_rules_array', 'filmakademy_add_rewrite_rules' );

/**
 * @param $post_link
 * @param $post
 * @return mixed|string
 * Custom replacement for URL on Film lists - We show the parent post
 */
function filmakademy_show_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'film' ):
        $post_parent_id = rwmb_meta( 'film-parent', '', $post->ID );
        if($post_parent_id != ''):
            $post_parent = get_post($post_parent_id);
            if( $post_parent && $post_parent->post_type == 'page' ):
                return str_replace( '%film_parent%' , $post_parent->post_name , $post_link );
            else:
                return 'film';
            endif;
        endif;
    endif;
    return $post_link;
}
add_filter( 'post_type_link', 'filmakademy_show_permalinks', 10, 2 );

/* Disable theme custom post types */
function filmakademy_remove_post_types() {
    unregister_post_type( 'slides' );
    unregister_post_type( 'carousels' );
    unregister_post_type( 'portfolio_page' );
}
add_action('init','filmakademy_remove_post_types', 100);